-- phpMyAdmin SQL Dump
-- version 5.2.0
-- https://www.phpmyadmin.net/
--
-- Hôte : 127.0.0.1
-- Généré le : mer. 14 juin 2023 à 05:49
-- Version du serveur : 10.4.27-MariaDB
-- Version de PHP : 8.2.0

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de données : `covoituragedb`
--

-- --------------------------------------------------------

--
-- Structure de la table `arrets`
--

CREATE TABLE `arrets` (
  `id` int(11) NOT NULL,
  `id_lieu_depart` int(11) DEFAULT NULL,
  `id_lieu_arrivee` int(11) DEFAULT NULL,
  `nom_arret` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Déchargement des données de la table `arrets`
--

INSERT INTO `arrets` (`id`, `id_lieu_depart`, `id_lieu_arrivee`, `nom_arret`) VALUES
(1, 1, 2, 'Place du marché'),
(2, 2, 3, 'Place des étudiants'),
(3, 3, 4, 'Rue principale');

-- --------------------------------------------------------

--
-- Structure de la table `conducteur`
--

CREATE TABLE `conducteur` (
  `id` int(11) NOT NULL,
  `date` date NOT NULL,
  `marque` varchar(10) NOT NULL,
  `numero` varchar(10) NOT NULL,
  `place` int(11) NOT NULL,
  `frais` varchar(10) NOT NULL,
  `depart` varchar(50) NOT NULL,
  `destination` varchar(50) NOT NULL,
  `heure` time NOT NULL,
  `info` varchar(255) NOT NULL,
  `chemin` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Déchargement des données de la table `conducteur`
--

INSERT INTO `conducteur` (`id`, `date`, `marque`, `numero`, `place`, `frais`, `depart`, `destination`, `heure`, `info`, `chemin`) VALUES
(52, '2023-06-07', 'MIA', '4334', 10, '1000', 'D', 'O', '08:00:00', 'Hey', 5),
(53, '0000-00-00', '', '', 0, '', 'O', 'D', '20:00:00', '', 6),
(54, '0000-00-00', '', '', 0, '', 'O', 'D', '20:00:00', '', 6),
(55, '2023-06-22', '', '', 0, '', 'E', 'H', '00:00:00', '', 0),
(56, '0000-00-00', '', '', 0, '', 'A', 'B', '00:00:00', 'hey', 0),
(57, '2023-06-05', 'Iari', '123455', 15, '600', 'E', 'H', '19:50:00', 'Salut charlie', 0),
(58, '0000-00-00', 'Halo', '', 0, '', 'A', 'B', '00:00:00', '', 5),
(59, '0000-00-00', '', '', 0, '', 'A', 'C', '00:00:00', 'Ndrendro', 5),
(60, '0000-00-00', '', '', 0, '', 'A', 'C', '00:00:00', 'Ca va', 5),
(61, '0000-00-00', '', '', 0, '', 'A', 'C', '00:00:00', '', 0);

-- --------------------------------------------------------

--
-- Structure de la table `lieux`
--

CREATE TABLE `lieux` (
  `id` int(11) NOT NULL,
  `nom` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Déchargement des données de la table `lieux`
--

INSERT INTO `lieux` (`id`, `nom`) VALUES
(1, 'Gare centrale'),
(2, 'Centre-ville'),
(3, 'Université'),
(4, 'Parc');

-- --------------------------------------------------------

--
-- Structure de la table `ma_table`
--

CREATE TABLE `ma_table` (
  `division` decimal(8,2) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

-- --------------------------------------------------------

--
-- Structure de la table `trajet`
--

CREATE TABLE `trajet` (
  `id` int(11) NOT NULL,
  `longueur` int(11) NOT NULL,
  `lieux` varchar(255) NOT NULL,
  `division` decimal(11,2) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Déchargement des données de la table `trajet`
--

INSERT INTO `trajet` (`id`, `longueur`, `lieux`, `division`) VALUES
(5, 6, 'O,A,B,C,D', '1.50'),
(6, 10, 'O,X,Y,Z,D', '2.50'),
(7, 8, 'O,E,F,B,H,D', '1.60'),
(8, 7, 'O,E,F,G,C,D', '1.40'),
(9, 7, 'O,E,A,I,H,D', '1.40'),
(10, 9, 'O,P,A,F,G,C,D', '1.50');

-- --------------------------------------------------------

--
-- Structure de la table `utilisateur`
--

CREATE TABLE `utilisateur` (
  `name` varchar(50) NOT NULL,
  `username` varchar(50) NOT NULL,
  `email` varchar(50) NOT NULL,
  `password` varchar(30) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Déchargement des données de la table `utilisateur`
--

INSERT INTO `utilisateur` (`name`, `username`, `email`, `password`) VALUES
('Ramahaliarivo Miantsa Iarilanja', '@miantsa', 'miantsaiarilanja@gmail.com', 'moisdemars4334'),
('jdfhkdjhf', 'jk', 'kj', 'jk'),
('Tsilavina', 'Tsila', 'miantsaiarilanja', 'jhfjdhfj');

--
-- Index pour les tables déchargées
--

--
-- Index pour la table `arrets`
--
ALTER TABLE `arrets`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id_lieu_depart` (`id_lieu_depart`),
  ADD KEY `id_lieu_arrivee` (`id_lieu_arrivee`);

--
-- Index pour la table `conducteur`
--
ALTER TABLE `conducteur`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `lieux`
--
ALTER TABLE `lieux`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `trajet`
--
ALTER TABLE `trajet`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `utilisateur`
--
ALTER TABLE `utilisateur`
  ADD PRIMARY KEY (`username`);

--
-- AUTO_INCREMENT pour les tables déchargées
--

--
-- AUTO_INCREMENT pour la table `arrets`
--
ALTER TABLE `arrets`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT pour la table `conducteur`
--
ALTER TABLE `conducteur`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=62;

--
-- AUTO_INCREMENT pour la table `lieux`
--
ALTER TABLE `lieux`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT pour la table `trajet`
--
ALTER TABLE `trajet`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- Contraintes pour les tables déchargées
--

--
-- Contraintes pour la table `arrets`
--
ALTER TABLE `arrets`
  ADD CONSTRAINT `arrets_ibfk_1` FOREIGN KEY (`id_lieu_depart`) REFERENCES `lieux` (`id`),
  ADD CONSTRAINT `arrets_ibfk_2` FOREIGN KEY (`id_lieu_arrivee`) REFERENCES `lieux` (`id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
