import React, { useState } from "react";

function Chemin() {
  const [lieuDepart, setLieuDepart] = useState("");
  const [lieuDestination, setLieuDestination] = useState("");
  const [trajets, setTrajets] = useState([]);

  const handleLieuDepartChange = (event) => {
    setLieuDepart(event.target.value);
  };

  const handleLieuDestinationChange = (event) => {
    setLieuDestination(event.target.value);
  };

  const handleSubmit = async (event) => {
    event.preventDefault();

    try {
      const response = await fetch(`/lieux?depart=${lieuDepart}&destination=${lieuDestination}`);
      const data = await response.json();
      setTrajets(data);
    } catch (error) {
      console.error("Erreur lors de la récupération des données:", error);
    }
  };

  return (
    <div>
      <h2>Sélectionnez le lieu de départ :</h2>
      <select value={lieuDepart} onChange={handleLieuDepartChange}>
        <option value="">Sélectionnez un lieu de départ</option>
        <option value="E">E</option>
        <option value="F">F</option>
        <option value="B">B</option>
        <option value="H">H</option>
      </select>

      <h2>Sélectionnez le lieu de destination :</h2>
      <select value={lieuDestination} onChange={handleLieuDestinationChange}>
        <option value="">Sélectionnez un lieu de destination</option>
        <option value="E">E</option>
        <option value="F">F</option>
        <option value="B">B</option>
        <option value="H">H</option>
      </select>

      <button onClick={handleSubmit}>Afficher les trajets</button>

      {trajets.length > 0 ? (
        <div>
          <h2>Liste des lieux entre {lieuDepart} et {lieuDestination} :</h2>
          <ul>
            {trajets.map((lieux, index) => (
              <li key={index}>{lieux}</li>
            ))}
          </ul>
        </div>
      ) : (
        <p>Aucun trajet trouvé entre les lieux sélectionnés.</p>
      )}
    </div>
  );
}

export default Chemin;
