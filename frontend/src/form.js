import axios from "axios";
import React, { useState } from 'react';
import { useNavigate } from "react-router-dom";

function Formulaire() {
  const [name, setName] = useState('');
  const [username, setUsername] = useState('');
  const [email, setEmail] = useState('');
  const [password, setPassword] = useState('');
  const navigate = useNavigate();

  const handleSubmit = (event) => {
    event.preventDefault();
    axios.post("http://localhost:3001/form", { name, username, email, password })
      .then(res => {
        console.log(res);
        navigate('/');
      })
      .catch(err => console.log(err));
  };

  return (
    <form onSubmit={handleSubmit}>
      <label htmlFor="">Nom</label>
      <input type="text" className="form-control" onChange={e => setName(e.target.value)} />
      <label htmlFor="">Username</label>
      <input type="text" className="form-control" onChange={e => setUsername(e.target.value)} />
      <label htmlFor="">email</label>
      <input type="text" className="form-control" onChange={e => setEmail(e.target.value)} />
      <label htmlFor="">Password</label>
      <input type="password" className="form-control" onChange={e => setPassword(e.target.value)} />
      <input type="submit" value="Submit" className="form-control" />
    </form>
  );
}

export default Formulaire;
