import 'bootstrap/dist/css/bootstrap.min.css';
import React from 'react';
import "./App.css";
import Nav from "react-bootstrap/Nav";
import Navbar from "react-bootstrap/Navbar";
import { Container, Row, Col, NavbarBrand } from "react-bootstrap";

function Navigation(){
    return(
        <Navbar className='content-navbar fixed-top'>
            <Container>
                <Navbar.Brand>Tomobil</Navbar.Brand>
                <Nav className="justify-content-end">
                    <Nav.Link href="#programme">Programme</Nav.Link>
                    <Nav.Link href="#conduire">Conduire</Nav.Link>
                    <Nav.Link href="#trajet">Trajet</Nav.Link>
                </Nav>
            </Container>
        </Navbar>
    )
}

export default Navigation;