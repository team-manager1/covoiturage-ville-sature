import React, { useState, useEffect } from 'react';
import axios from 'axios';
import moment from 'moment';

const YourComponent = () => {
  const [data, setData] = useState([]);
  const [currentDate, setCurrentDate] = useState(moment().format('YYYY-MM-DD'));

  useEffect(() => {
    fetchData(currentDate);
  }, [currentDate]);

  const fetchData = (date) => {
    axios.get(`http://localhost:3001/cond?date=${date}`)
      .then(res => setData(res.data))
      .catch(err => console.log(err));
  };

  const handleDateChange = (e) => {
    const selectedDate = e.target.value;
    setCurrentDate(selectedDate);
  };

  const filteredData = data.filter(item => moment(item.date).format('YYYY-MM-DD') === currentDate);

  const groupedData = filteredData.reduce((acc, item) => {
    const date = moment(item.date).format('YYYY-MM-DD');
    if (!acc[date]) {
      acc[date] = [item];
    } else {
      acc[date].push(item);
    }
    return acc;
  }, {});

  return (
    <div className="container">
      <h1 className="mt-4">Résultats de la requête</h1>
      <div className="form-group mt-4">
        <label htmlFor="datePicker">Sélectionner une date :</label>
        <input
          id="datePicker"
          type="date"
          className="form-control"
          value={currentDate}
          onChange={handleDateChange}
        />
      </div>
      <h2 className="mt-4">Données pour la date : {currentDate}</h2>
      {filteredData.length > 0 ? (
        <table className="table mt-4">
          <thead>
            <tr>
              <th scope="col">ID</th>
              <th scope="col">CHEMIN</th>
              <th scope="col">COUNT CHEMIN</th>
              <th scope="col">LIEUX</th>
              <th scope="col">DATE</th>
            </tr>
          </thead>
          <tbody>
            {filteredData.map(item => (
              <tr key={item.id}>
                <td>{item.id}</td>
                <td>{item.chemin}</td>
                <td>{item.count_chemin}</td>
                <td>{item.lieux}</td>
                <td>{item.date}</td>
              </tr>
            ))}
          </tbody>
        </table>
      ) : (
        <p className="mt-4">Aucun résultat trouvé pour la date sélectionnée.</p>
      )}

      {Object.entries(groupedData).map(([date, items]) => (
        <div key={date}>
          <h3>Date: {date}</h3>
          {items.length > 0 ? (
            <table className="table mt-2">
              <thead>
                <tr>
                  <th scope="col">ID</th>
                  <th scope="col">CHEMIN</th>
                  <th scope="col">COUNT CHEMIN</th>
                  <th scope="col">LIEUX</th>
                  <th scope="col">DATE</th>
                </tr>
              </thead>
              <tbody>
                {items.map(item => (
                  <tr key={item.id}>
                    <td>{item.id}</td>
                    <td>{item.chemin}</td>
                    <td>{item.count_chemin}</td>
                    <td>{item.lieux}</td>
                    <td>{item.date}</td>
                  </tr>
                ))}
              </tbody>
            </table>
          ) : (
            <p>Aucun résultat trouvé pour cette date.</p>
          )}
        </div>
      ))}
    </div>
  );
};

export default YourComponent;
