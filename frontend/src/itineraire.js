import React, { useEffect, useState } from "react";

function Itineraire() {
  const [lieux, setLieux] = useState([]);

  useEffect(() => {
    const fetchLieux = async () => {
      try {
        const response = await fetch("http://localhost:3001/lieux/6"); // Remplacez "4" par l'ID souhaité
        const data = await response.text();
        const parsedData = JSON.parse(data);
        setLieux(parsedData);
      } catch (error) {
        console.error("Erreur lors de la récupération des données:", error);
      }
    };

    fetchLieux();
  }, []);

  return (
    <div>
      <h2>Liste des lieux ch :</h2>
      <ul>
        {lieux.map((lieu, index) => (
          <li key={index}>{lieu.replace(/,/g, "")}</li>
        ))}
      </ul>
    </div>
  );
}

export default Itineraire;
