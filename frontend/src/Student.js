import React, { useEffect } from 'react'
import axios from "axios";
import Nav from 'react-bootstrap/Nav';

function Student(){

    useEffect(()=>{
        axios.get('http://localhost:3001/')
        .then(res => console.log(res))
        .catch(err => console.log(err));
    }, [])
    return (
    <Nav fill variant="tabs" defaultActiveKey="/home">
      <Nav.Item>
        <Nav.Link href="/home">Active</Nav.Link>
      </Nav.Item>
      <Nav.Item>
        <Nav.Link eventKey="link-1">Loooonger NavLink</Nav.Link>
      </Nav.Item>
      <Nav.Item>
        <Nav.Link eventKey="link-2">Link</Nav.Link>
      </Nav.Item>
      <Nav.Item>
        <Nav.Link eventKey="disabled" disabled>
          Disabled
        </Nav.Link>
      </Nav.Item>
    </Nav>
  );
}

export default Student;