import 'bootstrap/dist/css/bootstrap.min.css';
import Navigation from "./today";
import { Container, Row, Col } from 'react-bootstrap';
import Form from 'react-bootstrap/Form';
import React, { useState, useEffect } from 'react';
import Button from 'react-bootstrap/Button';
import axios from "axios";
import "./Programme.css";

function Programme(){
    const [liste, setListe] = useState([]);

    useEffect (()=>{
        axios.get("http://localhost:3001/programme")
        .then(res => setListe(res.data))
        .catch(err => console.log(err));
    },[])

    return(
        <>
            <Navigation />
            <Container className='bus bg-primary text-white' style={{marginTop:'60px'}}>
                <Row>
                    <Col><h4 className='text-white'>Aujourd'hui</h4></Col>
                    <Col style={{
                        borderLeft:'1px solid white',
                        textAlign:'center'
                    }}>   
                        <h4 className='text-white'>BUS</h4> 
                    </Col>
                </Row>
            </Container>
            <div as={Col} style={{width:"70%", marginLeft:"20px"}}>
                <table style={{
                    fontFamily:"Trebuchet MS",
                    borderCollapse:'collapse',
                    width:'100%'
                }}>
                    <thead>
                        <th>Heure de Depart</th>
                        <th>Lieu de Depart</th>
                        <th>Destination</th>
                    </thead>
                    <tbody>
                    {liste && liste.map((data, i) => {
                        return (
                        <tr key={i}>
                            <td>{data.heure}</td>        
                            <td>{data.depart}</td>
                            <td>{data.destination}</td>
                            <td><Button variant='outline-primary'>Reserver</Button>
                            <Button variant='outline-secondary'>Details</Button></td>
                        </tr>
                        );
                    })}

                </tbody>
                </table>
            </div> 
            <div>
                
            </div>               
        </>
    );
}

export default Programme;