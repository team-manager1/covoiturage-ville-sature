import React, { useEffect, useRef } from 'react';
import L from 'leaflet';
import 'leaflet-routing-machine';
import 'leaflet/dist/leaflet.css';

const Map = () => {
  const mapRef = useRef(null);
  const routingControlRef = useRef(null);

  useEffect(() => {
    if (!mapRef.current) {
      // Création de la carte si elle n'a pas déjà été initialisée
      mapRef.current = L.map('map').setView([-21.4536, 47.0856], 13);

      // Ajout des tuiles de fond de la carte depuis OpenStreetMap
      L.tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
        attribution: 'Map data &copy; <a href="https://www.openstreetmap.org/">OpenStreetMap</a> contributors',
        maxZoom: 18,
      }).addTo(mapRef.current);
    }
  }, []);

  const calculateRoute = () => {
    if (routingControlRef.current) {
      mapRef.current.removeControl(routingControlRef.current);
    }

    const routingControl = L.Routing.control({
      waypoints: [
        L.latLng(-21.4536, 47.0856), // Départ (exemple)
        L.latLng(-21.4563, 47.0898), // Arrivée (exemple)
      ],
      routeWhileDragging: true,
    }).addTo(mapRef.current);

    routingControlRef.current = routingControl;
  };

  return (
    <div>
      <div id="map" style={{ height: '400px' }}></div>
      <button onClick={calculateRoute}>Calculer l'itinéraire</button>
    </div>
  );
};

export default Map;
