import React, { useEffect, useRef, useState } from 'react';
import L from 'leaflet';
import 'leaflet-routing-machine';
import 'leaflet/dist/leaflet.css';

const Map = () => {
  const mapRef = useRef(null);
  const routingControlRef = useRef(null); // Référence au contrôle de calcul d'itinéraire
  const [depart, setDepart] = useState('');
  const [arrivee, setArrivee] = useState('');

  useEffect(() => {
    if (!mapRef.current) {
      // Création de la carte si elle n'a pas déjà été initialisée
      mapRef.current = L.map('map').setView([-21.4536, 47.0856], 13);

      // Ajout des tuiles de fond de la carte depuis OpenStreetMap
      L.tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
        attribution: 'Map data &copy; <a href="https://www.openstreetmap.org/">OpenStreetMap</a> contributors',
        maxZoom: 18,
      }).addTo(mapRef.current);
    }

    // Mettre à jour l'itinéraire lorsque les lieux de départ et d'arrivée sont définis
    if (depart && arrivee) {
      if (routingControlRef.current) {
        // Supprimer l'itinéraire précédent s'il existe
        routingControlRef.current.removeFrom(mapRef.current);
      }

      const routingControl = L.Routing.control({
        waypoints: [
          L.latLng(depart),
          L.latLng(arrivee),
        ],
        routeWhileDragging: true,
      }).addTo(mapRef.current);

      routingControlRef.current = routingControl; // Mise à jour de la référence au contrôle de calcul d'itinéraire
    }
  }, [depart, arrivee]);

  const handleSubmit = (e) => {
    e.preventDefault();
    // Mettez à jour les valeurs des champs de saisie dans l'état
    setDepart(e.target.depart.value);
    setArrivee(e.target.arrivee.value);
  };

  return (
    <div>
      <form onSubmit={handleSubmit}>
        <label htmlFor="depart">Lieu de départ:</label>
        <input type="text" id="depart" name="depart" />

        <label htmlFor="arrivee">Lieu d'arrivée:</label>
        <input type="text" id="arrivee" name="arrivee" />

        <button type="submit">Calculer l'itinéraire</button>
      </form>
      <div id="map" style={{ height: '400px' }}></div>
    </div>
  );
};

export default Map;
