import 'bootstrap/dist/css/bootstrap.min.css';
import Nav from 'react-bootstrap/Nav';
import './App.css';
import { Container, Row, Col, Alert } from 'react-bootstrap';
import Toast from 'react-bootstrap/Toast';
import React, { useState } from 'react';
import Button from 'react-bootstrap/Button';
import axios from "axios";
import Navbar from 'react-bootstrap/Navbar';
import FloatingLabel from 'react-bootstrap/FloatingLabel';
import Form from 'react-bootstrap/Form';
import Modal from 'react-bootstrap/Modal';
import { useNavigate } from 'react-router-dom';

export function Onglet({ toggleShowLoginForm }) {
  return (
    <Navbar className='navbar' variant="dark">
      <Container>
        <Navbar.Brand href="/"><h1>Tomobil</h1></Navbar.Brand>
        <Navbar.Toggle />
        <Nav className="justify-content-end" defaultActiveKey="/home">
          <Nav.Item>
            <Nav.Link href="#" className="text-white">Covoiturage</Nav.Link>
          </Nav.Item>
          <Nav.Item>
            <Button onClick={toggleShowLoginForm} className="mb-3 text-white">Se connecter</Button>
          </Nav.Item>
        </Nav>
      </Container>
    </Navbar>
  );
}

function Description() {
  const [showA, setShowA] = useState(false);

  const toggleShowA = () => setShowA(!showA);

  return (
    <Row>
      <Col md={6} className="mb-2">
        <Button variant="outline-secondary" onClick={toggleShowA} className="mb-2" style={{ color: 'white' }}>
          En savoir plus
        </Button>
        <Toast show={showA}>
          <Toast.Body className='bg-info text-black'>
          Notre site de covoiturage met en relation les conducteurs et les passagers qui souhaitent partager un trajet commun. Que vous cherchiez un moyen abordable de vous déplacer ou que vous ayez des places libres dans votre voiture, nous avons la solution idéale pour vous
          </Toast.Body>
        </Toast>
      </Col>
    </Row>
  );
}

function Home() {
  const [showLoginForm, setShowLoginForm] = useState(false);
  const [showSigninForm, setShowSigninForm] = useState(false);

  const toggleShowLoginForm = () => {
    setShowLoginForm(!showLoginForm);
  };
  const toggleShowSigninForm = () => {
    setShowSigninForm(!showSigninForm);
  };

  const [name, setName] = useState('');
  const [username, setUsername] = useState('');
  const [email, setEmail] = useState('');
  const [password, setPassword] = useState('');
  const navigate = useNavigate();

  const handleSubmit = (event) => {
    event.preventDefault();
    axios.post("http://localhost:3001/signin", { name, username, email, password })
      .then(res => {
        console.log(res);
        navigate('/');
      })
      .catch(err => console.log(err));
  };

  const handleLogin = (event) => {
    event.preventDefault();
    axios
      .post("http://localhost:3001/login", { username, password })
      .then((res) => {
        if (res.status === 200) {
          console.log("Connexion réussie");
          navigate('/covoiturage');
          // Effectuer une action après une connexion réussie (redirection, affichage de messages, etc.)
        } else {
          console.log("Connexion refusée");
          // Afficher un message d'erreur ou effectuer une autre action pour gérer l'authentification non valide
        }
      })
      .catch((err) => console.log(err));
  };
  

  return (
    <>
      <Onglet toggleShowLoginForm={toggleShowLoginForm} />
      <div className="w-100">
        <img src="/images/covoiturage.jpeg" alt='money' className='img' style={{ width: '100%', height: '100vh' }} />

        <div className="overlay">
                <div className="text-wrapper">
                  <h1>Bienvenue sur notre plateforme de covoiturage !</h1>
                  <h3>Découvrez une nouvelle façon de voyager tout en réduisant votre empreinte carbone.</h3>
                  <Description />
                </div>
        </div>
      </div>
      <Modal show={showLoginForm} onHide={toggleShowLoginForm}>
        <Modal.Header closeButton>
          <Modal.Title>Se connecter</Modal.Title>
        </Modal.Header>
        <Modal.Body>
          <Form className="login" style={{ padding: "20px" }}>
            <FloatingLabel controlId="floatingInput" label="Nom d'utilisateur" className="mb-2 text-black">
              <Form.Control type="text" placeholder="@utilisateur" className='input' 
              onChange={e=> setUsername(e.target.value)}/>
            </FloatingLabel>
            <FloatingLabel controlId="floatingPassword" label="Mot de passe" className='mb-2'>
              <Form.Control type="password" placeholder="Password" className='input' 
              onChange={e=> setPassword(e.target.value)}/>
            </FloatingLabel>
            <Button variant="primary" onClick={handleLogin}>Se connecter</Button>
          </Form>
        </Modal.Body>
        <Modal.Footer>
          <Container>
            <Row>
              <Col sm={6}>
              <Button variant="outline-primary" onClick={toggleShowSigninForm}>S'inscrire</Button>
              </Col>
              <Col sm={2}>
                <Button variant="secondary" onClick={toggleShowLoginForm}>Fermer</Button>
              </Col>
              <Col sm={4}>
                <Button variant="primary" onClick={toggleShowLoginForm}>Se connecter</Button>
              </Col>
            </Row>
          </Container>
        </Modal.Footer>
      </Modal>
      <Modal show={showSigninForm} onHide={toggleShowSigninForm}>
        <Modal.Header closeButton>
          <Modal.Title>S'inscrire</Modal.Title>
        </Modal.Header>
        <Modal.Body>
          <Form className="login">
            <FloatingLabel controlId="floatingInput" label="Nom et prenoms" className="mb-2 text-black">
              <Form.Control type="text" placeholder="Nom et prenom" className='input' 
              onChange={e => setName(e.target.value)}/>
            </FloatingLabel>
            <FloatingLabel controlId="floatingInput" label="Nom d'utilisateur" className="mb-2 text-black">
              <Form.Control type="text" placeholder="Nom d'utilisateur" className='input' 
              onChange={e => setUsername(e.target.value)}/>
            </FloatingLabel>
            <FloatingLabel controlId="floatingInput" label="Email" className="mb-2 text-black">
              <Form.Control type="text" placeholder="Adresse email" className='input' 
              onChange={e => setEmail(e.target.value)}/>
            </FloatingLabel>
            <FloatingLabel controlId="floatingPassword" label="Mot de passe" className='mb-2'>
              <Form.Control type="password" placeholder="Password" className='input' 
              onChange={e => setPassword(e.target.value)}/>
            </FloatingLabel>
            <Button variant="primary" onClick={handleSubmit}>S'inscrire</Button>
          </Form>
        </Modal.Body>
        <Modal.Footer>
          <Container>
            <Row>
              <Col sm={8}>
                <p>Vous avez deja un compte ? </p>
              </Col>
              <Col sm={4}>
                <Button variant="outline-primary" onClick={toggleShowSigninForm}>Se connecter</Button>
              </Col>
            </Row>
          </Container>
        </Modal.Footer>
      </Modal>
    </>
  );
}

export default Home;