import React, { useState, useEffect } from 'react';
import Button from 'react-bootstrap/Button';
import Col from 'react-bootstrap/Col';
import Form from 'react-bootstrap/Form';
import Row from 'react-bootstrap/Row';
import Navigation from "./today";
import "./conducteur.css";
import { Container, InputGroup} from 'react-bootstrap';
import Nav from 'react-bootstrap/Nav';
import axios from "axios";
import { useNavigate } from 'react-router-dom';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faUser, faEnvelope, faBook, faNewspaper } from '@fortawesome/free-solid-svg-icons';

function Conduire() {
  const [agenda, setAgenda] = useState([]);
  const [lieu, setLieu] = useState([]);
  const [depart, setDepart] = useState('');
  const [destination, setDestination] = useState('');

  useEffect(() => {
    axios.get("http://localhost:3001/conduct")
      .then(res => setAgenda(res.data))
      .catch(err => console.log(err));
  }, [])

  useEffect(() => {
    if (depart && destination) {
      axios.post("http://localhost:3001/trajet", { depart, destination })
        .then((res) => {
          if (res.status === 200) {
            console.log("Connexion réussie");
            setLieu(res.data);
          } else {
            console.log("Connexion refusée");
            // Afficher un message d'erreur ou effectuer une autre action pour gérer l'authentification non valide
          }
        })
        .catch((err) => console.log(err));
    }
  }, [depart, destination]);

  const [date, setDate] = useState('');
  const [marque, setMarque] = useState('');
  const [numero, setNumero] = useState('');
  const [place, setPlace] = useState('');
  const [frais, setFrais] = useState('');
  const [heure, setHeure] = useState('');
  const [info, setInfo] = useState('');
  const [choixChemin, setChoixChemin] = useState([]);
  const navigate = useNavigate();

  const handleSubmit = (event) => {
    event.preventDefault();
    axios
      .post("http://localhost:3001/conducteur", { date, marque, numero, place, frais, depart, destination, heure, info, choixChemin })
      .then((res) => {
        if (res.status === 200) {
          console.log("Connexion réussie");
          console.log(choixChemin)
          navigate('/conducteur');
          window.location.reload();
          // Effectuer une action après une connexion réussie (redirection, affichage de messages, etc.)
        } else {
          console.log("Connexion refusée");
          // Afficher un message d'erreur ou effectuer une autre action pour gérer l'authentification non valide
        }
      })
      .catch((err) => console.log(err));
  };

  return (
    <>
      <Navigation />
      <Container style={{ marginTop: '50px' }}>
        <Row>
          <Col className='menu' style={{
            height: '100vh',
            position: 'fixed',
            padding: '20px 0',
            textAlign: "center",
            width: '8%',
            left: '0'
          }}>
            <Nav.Link href="/home" className='text-white' style={{ marginBottom: '20px' }}>
              <FontAwesomeIcon icon={faUser} style={{ height: '30px' }} /><br />
              Profil
            </Nav.Link>
            <Nav.Link eventKey="link-1" className='text-white' style={{ marginBottom: '20px' }}>
              <FontAwesomeIcon icon={faEnvelope} style={{ height: '30px' }} /><br />
              Messages
            </Nav.Link>
            <Nav.Link eventKey="link-2" className='text-white' style={{ marginBottom: '20px' }}>
              <FontAwesomeIcon icon={faBook} style={{ height: '30px' }} /><br />
              Reservation
            </Nav.Link>
            <Nav.Link eventKey="link-2" className='text-white' style={{ marginBottom: '20px' }}>
              <FontAwesomeIcon icon={faNewspaper} style={{ height: '30px' }} /><br />
              Journal
            </Nav.Link>
          </Col>
          <Col sm={8} style={{
            height: '100vh'
          }}>
            <Form style={{
              padding: '20px',
              marginLeft: '3%',
            }}>
              <br />
              <Form.Group as={Row} className="mb-3" controlId="formHorizontalEmail">
                <Col sm={6}><h3>Publication de trajet</h3></Col>
                <Form.Label as={Col} style={{ textDecoration: 'underline', padding: '5px 0' }}>
                  Date de conduite :
                </Form.Label>
                <Col>
                  <Form.Control type="Date" placeholder="Date de conduite"
                    onChange={e => setDate(e.target.value)} />
                </Col>
              </Form.Group>
              <Row>
                <h4>Voiture</h4><hr />
                <Form.Group as={Col} controlId='marque'>
                  <Form.Label>Marque</Form.Label>
                  <Form.Control type='text' placeholder='Marque de votre voiture'
                    onChange={e => setMarque(e.target.value)} />
                </Form.Group>
                <Form.Group as={Col} controlId='numVoiture'>
                  <Form.Label>Numero de la voiture</Form.Label>
                  <Form.Control type='text' placeholder='votre numero de voiture'
                    onChange={e => setNumero(e.target.value)} />
                </Form.Group>
                <Form.Group as={Col} controlId='place'>
                  <Form.Label>Nombre de place</Form.Label>
                  <Form.Control type='number' placeholder=''
                    onChange={e => setPlace(e.target.value)} />
                </Form.Group>
                <Form.Group as={Col} controlId='frais'>
                  <Form.Label>Frais</Form.Label>
                  <Form.Control type='text' placeholder=''
                    onChange={e => setFrais(e.target.value)} />
                </Form.Group>
              </Row>
              <br />
              <Row>
                <h4>Details du trajet</h4><hr />
                <Form.Group as={Col} controlId='depart'>
                  <Form.Label>Lieu de Départ</Form.Label>
                  <Form.Control as="select" onChange={e => setDepart(e.target.value)}>
                    <option value="">Sélectionnez votre lieu de départ</option>
                    <option value="A">A</option>
                    <option value="B">B</option>
                    <option value="C">C</option>
                    <option value="D">D</option>
                    <option value="E">E</option>
                    <option value="F">F</option>
                    <option value="G">G</option>
                    <option value="H">H</option>
                    <option value="I">I</option>
                    <option value="P">P</option>
                    <option value="X">X</option>
                    <option value="Y">Y</option>
                    <option value="Z">Z</option>
                    <option value="O">O</option>
                  </Form.Control>
                </Form.Group>
                <Form.Group as={Col} controlId='destination'>
                  <Form.Label>Destination</Form.Label>
                  <Form.Control as="select" onChange={e => setDestination(e.target.value)}>
                    <option value="">Sélectionnez votre destination</option>
                    <option value="A">A</option>
                    <option value="B">B</option>
                    <option value="C">C</option>
                    <option value="D">D</option>
                    <option value="E">E</option>
                    <option value="F">F</option>
                    <option value="G">G</option>
                    <option value="H">H</option>
                    <option value="I">I</option>
                    <option value="P">P</option>
                    <option value="X">X</option>
                    <option value="Y">Y</option>
                    <option value="Z">Z</option>
                    <option value="O">O</option>
                  </Form.Control>
                </Form.Group>
                <Form.Group as={Col} controlId='heure'>
                  <Form.Label>Heure de Depart</Form.Label>
                  <Form.Control type='time' placeholder='votre horaire de depart'
                    onChange={e => setHeure(e.target.value)} />
                </Form.Group>
              </Row><br/>
              <Row>
              <h5>Itineraires</h5>
                {lieu.map((result, i) => {
                    return (  
                    <>
                    <Row>
                    <Col>
                    <input type='radio' name="chemin" value={result.id} onChange={e=>setChoixChemin(e.target.value)}/>        
                    <ul key={i}>
                        {result.itineraire.split(',').map((lieu, j) => (
                        <li key={j} style={{
                            listStyleType:'none',
                            display:'inline-block',
                            padding:'0 5px'}}>
                            {lieu}
                        </li>
                        ))}
                        <input type="text" value={result.distance}/>
                    </ul>
                    </Col>
                    </Row>
                    </>
                    );
                })}
              </Row>
              <br />
              <Row>
                <h4>Autres informations dédiées aux passagers</h4><hr />
                <Form.Group controlId="information">
                  <Form.Control as="textarea" rows={3}
                    onChange={e => setInfo(e.target.value)}/>
                </Form.Group>
              </Row>
              <Button variant='primary' onClick={handleSubmit}>Publier</Button>
            </Form>
          </Col>
          <Col sm={3} style={{ padding: '20px 10px'}}>
            <div className='agenda'>
              <h4 style={{ textAlign: "center"}}>Agenda</h4>
              <table className='table'>
                <thead>
                  <tr>
                    <th>Date</th>
                    <th>Origine</th>
                    <th>Destination</th>
                    <th>Heure</th>
                  </tr>
                </thead>
                <tbody>
                  {agenda.map((data, i) => {
                    const dateFormatted = new Date(data.date).toLocaleDateString("fr-FR", {
                      day: "numeric",
                      month: "short",
                      year: "numeric"
                    });
                    return (
                      <tr key={i}>
                        <td>{dateFormatted}</td>
                        <td>{data.depart}</td>
                        <td>{data.destination}</td>
                        <td>{data.heure}</td>
                      </tr>
                    );
                  })}
                </tbody>
              </table>
            </div>
          </Col>
        </Row>
      </Container>
    </>
  );
}

export default Conduire;