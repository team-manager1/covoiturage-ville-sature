import 'bootstrap/dist/css/bootstrap.min.css';
import {BrowserRouter, Routes, Route} from 'react-router-dom';
import Home from "./home";
import Map from "./test";
import Programme from "./programme";
import Conduire from "./conducteur";
import YourComponent  from "./trajet";
import Itineraire from "./itineraire";
import Chemin from "./chemin";

function App() {
  return (
    <div className="App">
      <BrowserRouter>
        <Routes>
          <Route path="/" element={<Home />}></Route>
          <Route path="/test" element={<Map />}></Route>
          <Route path="/programme" element={<Programme />}></Route>
          <Route path="/conducteur" element={<Conduire />}></Route>
          <Route path="/trajet" element={<YourComponent  />}></Route>
          <Route path="/itineraire" element={<Itineraire />}></Route>
          <Route path="/chemin" element={<Chemin />}></Route>
        </Routes>
      </BrowserRouter>
    </div>
  )
}

export default App;


