const express = require("express");
const cors = require("cors");
const mysql = require("mysql");
const app = express();

app.use(cors());
app.use(express.json());

const db = mysql.createConnection({
    host:"localhost",
    user:"root",
    password:"",
    database:"covoituragedb"
})

db.connect((err) => {
    if (err) {
        console.error("Erreur de connexion à la base de données : ", err);
    } else {
        console.log("Connexion à la base de données réussie !");
    }
});

const PORT = process.env.PORT || 3001;

app.post("/signin", (req, res) => {
    const { name, username, email, password } = req.body;

    const sql = "INSERT INTO utilisateur (name, username, email, password) VALUES (?, ?, ?, ?)";
    db.query(sql, [name, username, email, password], (err, result) => {
        if (err) {
            console.error("Erreur lors de l'enregistrement des données : ", err);
            res.sendStatus(500); // Réponse d'erreur interne du serveur
        } else {
            console.log("Données enregistrées avec succès !");
            res.sendStatus(200); // Réponse de succès
        }
    });
});

app.post("/conducteur", (req, res) => {
  const { date, marque, numero, place, frais, depart, destination, heure, info, choixChemin } = req.body;

  const sql = "INSERT INTO conducteur (date, marque, numero, place, frais, depart, destination, heure, info, chemin) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?)";
  db.query(sql, [date, marque, numero, place, frais, depart, destination, heure, info, choixChemin], (err, result) => {
    if (err) {
      console.error("Erreur lors de l'enregistrement des données : ", err);
      res.sendStatus(500); // Réponse d'erreur interne du serveur
    } else {
      console.log("Données enregistrées avec succès !");
      res.sendStatus(200); // Réponse de succès
    }
  });
});


app.post("/chemin", (req,res)=>{
  const {choixChemin} = req.body;
  const sql = "INSERT INTO conducteur(chemin) VALUE (?)";
  db.query(sql,[choixChemin],(err,data)=>{
    if (err) {
      console.error("Erreur lors de l'enregistrement des données : ", err);
      res.sendStatus(500); // Réponse d'erreur interne du serveur
  } else {
      console.log("Données enregistrées avec succès !");
      console.log(data);
      res.sendStatus(200); // Réponse de succès
  }
  })
})

app.get("/conduct", (req, res)=>{
  const sql = "SELECT * FROM conducteur LIMIT 5";
  db.query(sql, (err, data)=>{
    if(err) return res.json("Error");
    else{
      if(data.length > 0){
        console.log("resultat trouvé");
        return res.json(data);
      }else
      console.log("resultat vide");
    }
  })
})

app.get("/programme", (req, res)=>{
  const sql = "SELECT * FROM conducteur ORDER BY date,heure";
  db.query(sql, (err, result)=>{
    if(err) return res.json("Error");
    else{
      if(result.length > 0){
        console.log("resultat trouvé");
        return res.json(result);
      }else
      console.log("resultat vide");
    }
  })
})

app.post("/login", (req, res) => {
    const { username, password } = req.body;
  
    const sql = "SELECT password FROM utilisateur WHERE username = ?";
    db.query(sql, [username], (err, result) => {
      if (err) {
        console.error("Erreur", err);
        res.sendStatus(500);
      } else {
        if (result.length > 0 && result[0].password === password) {
          console.log("Connexion réussie");
          res.sendStatus(200);
        } else {
          console.log("Connexion refusée");
          res.sendStatus(401);
        }
      }
    });
})

app.post("/trajet", (req, res) => {
  const { depart, destination } = req.body;

  const sql = `SELECT IF(INSTR(lieux, ?) < INSTR(lieux, ?), SUBSTRING(lieux, INSTR(lieux, ?), INSTR(lieux, ?) - INSTR(lieux, ?) + 1),SUBSTRING(lieux, INSTR(lieux, ?), INSTR(lieux, ?) - INSTR(lieux, ?) + 1)) AS itineraire, id,((LENGTH(
    REPLACE(
        CONCAT(
            IF(INSTR(lieux, ?) < INSTR(lieux, ?), 
               SUBSTRING(lieux, INSTR(lieux, ?), INSTR(lieux, ?) - INSTR(lieux, ?) + 1),
               SUBSTRING(lieux, INSTR(lieux, ?), INSTR(lieux, ?) - INSTR(lieux, ?) + 1)
            )
        )
        , ',' , '')
)-1)*division) as distance 
FROM trajet
  WHERE lieux LIKE ?
    AND lieux LIKE ?
`;

  db.query(
    sql,
    [
      depart,
      destination,
      depart,
      destination,
      depart,
      destination,
      depart,
      destination,
      depart,
      destination,
      depart,
      destination,
      depart,
      destination,
      depart,
      destination,
      `%${depart}%`,
      `%${destination}%`,
    ],
    (err, result) => {
      if (err) {
        console.error("Erreur", err);
        res.sendStatus(500);
      } else {
        if (result.length > 0) {
          console.log(result);
          return res.json(result);
        } else {
          console.log("Résultat vide");
          res.sendStatus(404);
        }
      }
    }
  );
});

  
  app.get("/lieux/:id", (req, res) => {
    const { id } = req.params;
    const query = "SELECT lieux FROM trajet WHERE id = ?";
    db.query(query, [id], (error, results) => {
      if (error) {
        console.error("Erreur lors de l'exécution de la requête :", error);
        res.status(500).json({ error: "Erreur lors de la récupération des données" });
        return;
      }
  
      if (results.length === 0) {
        res.status(404).json({ error: "Aucun trajet trouvé avec l'ID spécifié" });
        return;
      }
  
      const lieux = results[0].lieux.split(",");
      res.json(lieux);
    });
  });

  app.get('/cond', (req, res) => {
    const sql1 = "SELECT t.id, c.chemin, t.lieux, COUNT(c.chemin) AS count_chemin FROM trajet AS t JOIN conducteur AS c ON t.id = c.chemin GROUP BY t.id, c.chemin, c.date ORDER BY c.date ASC ";
  
    db.query(sql1, (err, result) => {
      if (err) {
        console.error(err);
        return res.json("Error");
      } else {
        if (result.length > 0) {
          console.log("Résultats mis à jour dans la table trajet");
          console.log(result);
          return res.json(result);
        } else {
          console.log("Aucun résultat trouvé");
          return res.json([]);
        }
      }
    });
  });
  
  
app.listen(PORT, () => {
    console.log(`Server listening on ${PORT}`);
});